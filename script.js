const tabsLi = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');

tabsLi.forEach(function(item){
    item.addEventListener('click', function(){
        let currentLi = item;
        let tabId = currentLi.getAttribute('data-tab');
        let currentTab = document.querySelector(tabId);

        if(!currentLi.classList.contains('active')){
            tabsLi.forEach(function(item){
                item.classList.remove('active');
            });
    
            tabsItems.forEach(function (item){
                item.classList.remove('active');
            });
    
            currentLi.classList.add('active');
            currentTab.classList.add('active');
        }
    });
});

document.querySelector('.tabs-title').click();